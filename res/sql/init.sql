-- psql -U postgres < init.sql

-- создать базу данных
create database {{dbname}}
with
  owner = {{user}}
  encoding = 'Unicode'
;

\connect {{dbname}};

-- создать таблицу для хранения карточек
create table if not exists {{table}} (
  {{table}}_id uuid unique primary key,
  lastname text not null,
  firstname text not null,
  middlename text,
  birth date not null,
  death date not null,
  category int not null,
  bio text,
  image text
);

grant all on table public.afgan to leofedyaev;
