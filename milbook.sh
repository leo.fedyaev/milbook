#!/usr/bin/env sh

SELF=`readlink -f $0`
SELF_DIR=`echo "$SELF" | sed -e "s/\/$(basename "$SELF")$//g"`

"$SELF_DIR"/.venv/bin/python "$SELF_DIR/src/Application.py" "$@"
