# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import re
import textwrap

from jinja2 import Template
from datetime import datetime, date

from Card import Card
from task.Task import Task


#
class LatexFormat:
    """Подготовка текста для вёрстки в формате ~Latex~"""

    def __init__(self, card: Card):
        self.card = card

    def format(self, key: str) -> str:
        I = Template(textwrap.dedent(r'''
            \begin<wrapfigure>[9]<l><20mm>
            \includegraphics[width=20mm]<{{img}}>
            \end<wrapfigure>'''))

        T = Template(textwrap.dedent(r'''
            \noindent\fio<{{last}} {{first}} {{middle}}>
            ({{cbirth}} -- {{cdeath}}), {{text}}

            \medskip'''))

        img = I.render({'img': key}) if len(key) else ''

        text = T.render(
            {**self.card.__dict__,
             'last': self.card.last.upper(),
             'cbirth': self.card.birth.strftime('%d.%m.%Y'),
             'cdeath': self.card.death.strftime('%d.%m.%Y'),
             'text': self._corr(self.card.bio)})

        img = re.sub(r'\<', r'{', img, re.M)
        img = re.sub(r'\>', r'}', img, re.M)
        text = re.sub(r'\<', r'{', text, re.M)
        text = re.sub(r'\>', r'}', text, re.M)

        return (img + text)

    #
    def _corr(self, value: str) -> str:

        text = re.sub(r'^> ', r'', value, re.M)
        text = re.sub(r'\n', r'\n\n', text, re.M)
        text = re.sub(r'(^|\s)-(\s)', r'\1---\2', text, re.M)
        text = re.sub(r'(^|\s)—(\s)', r'\1---\2', text, re.M)
        text = re.sub(r'(^|\s)№(\s)', r'\1\\No\,', text, re.M)
        text = re.sub(r'(^|\s)г.(\s)', r'\1г.~', text, re.M)

        # автоформатирование дат к формату: %d.%m.%Y
        text = re.sub(r'(\d{1,2}).(\d{1,2}).(\d{4})',
                      lambda x: f'{int(x[1]):02d}.{int(x[2]):02d}.{x[3]}', text, re.M)

        return text
