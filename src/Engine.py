# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import re
import sys

from addict import Dict
from pathlib import Path
from datetime import datetime, date
from rich import print as pprint

from Card import Card
from Label import Label


#
class Engine:
    """Общий класс с реализацией всех поисковых алгоритмов"""

    def __init__(self, data: Dict, args: Dict, config: Dict):
        self.data = data
        self.args = args
        self.config = config


    #
    def find(self) -> Dict:
        """Найти все записи, удовлетворяющие заданным параметрам.

        Параметры:
        - name: list[str] = '', регулярное выражения для фильтрации по фамилии, 
            имени, отчеству;
        - birth: str = None, выражение для поиску по дате рождения;
        - death: str = None, выражение для поиску по дате смерти;
        - age: str = None, выражение для поиску по возрвсту;
        - male: bool = None, критерий для фильтрации по половой принадлежности;
        - categoty: int = None, критерий для фильтрации по категориальной 
            принадлежности;

        Возвращаемое значение:
        - dict, набор записей, удовлетворяющий заданным критериям поиска;
        """

        sign = {'>': 1, '<': -1, '=': 0}
        svalid = ['>', '<', '=', '>=', '<=']
        maskName = dict((k, Engine._rstr(v)) for k, v in enumerate(self.args.name))

        #
        def __empty(empty: bool) -> bool:
            """Фильтрация по признаку заполненности карточки"""

            if not self.args.not_empty:
                return True

            return self.args.not_empty == (not empty)

        #
        def __name(last: str, first: str, middle: str) -> bool:
            """Фильтрация по фамилии, имени, отчеству"""

            return \
                re.match(maskName.get(0, ''), last, re.I) and \
                re.match(maskName.get(1, ''), first, re.I) and \
                re.match(maskName.get(2, ''), middle, re.I)

        #
        def __gender(male: bool) -> bool:
            """Фильтрация по половой принадлежности"""

            return male == {'all': True, 'male': True, 'female': False}[self.args.gender]

        #
        def __category(cat: int) -> bool:
            """Фильтрация по категории"""

            if self.args.category is None:
                return True

            return cat == self.args.category

        #
        def __cmp(a: int, b: int) -> int:
            """Сравнение двух чисел"""

            return (a > b) - (a < b)

        #
        def __stamp(d: date) -> int:
            """Преобразовать дату в число"""

            return d.year + d.month + d.day

        #
        def __filter(v, exp: str, _cast: callable, _cmp: callable) -> bool:
            """Фильтрация по выражению-предикату"""

            if exp is None:
                return True

            _s, _p = exp.split()

            if _s not in svalid:
                pprint(f'{Label.H.error} Неизвестный предикат "{_s}", допустимы: {svalid};')
                sys.exit(-2)

            _p = _cast(_p)

            res = None

            for it in list(_s):
                _r = sign[it] == _cmp(v, _p)
                res = _r if res is None else (res or _r)

            return res

        #
        def __date(d: date, exp: str) -> bool:
            """Фильтрация по дате"""

            return __filter(d, exp,
                            lambda x: datetime.strptime(x, '%d-%m-%Y').date(),
                            lambda a, b: __cmp(__stamp(a), __stamp(b)))

        #
        def __age(age: int) -> bool:
            """Фильтрация по возрасту"""

            return __filter(age, self.args.age,
                            lambda x: int(x), lambda a, b: __cmp(a, b))

        #
        def __foto(key: str) -> bool:
            """Фильтрация по фотографии"""

            if self.args.foto == 'both':
                return True

            F = {'yes': lambda x: x, 'no': lambda x: not x}
            exists = Path(f'{self.config.dirs.res}/{Path(key).stem}.png').exists()

            return F[self.args.foto](exists)

        ##
        def __match(key: str, card: Card) -> bool:
            """Комплексное сравнение"""

            return __empty(card.empty()) and\
                __name(card.last, card.first, card.middle) and\
                __gender(card.male) and __category(card.category) and\
                __date(card.death, self.args.death) and\
                __date(card.birth, self.args.birth) and\
                __age(card.age()) and __foto(key)


        # поиск (фильтрация) по заданным критериям
        found = self.sort(dict((k, v) for k, v in self.data.items() if __match(k, v)))

        if self.args.head is not None or self.args.tail is not None:
            # сократить результаты до заданного количества --head, --tail
            head = dict(list(found.items())[:self.args.head])\
                if self.args.head is not None else {}

            tail = dict(list(found.items())[len(found.items()) - self.args.tail:])\
                if self.args.tail is not None else {}

            head.update(tail)

            return self.sort(head)
        else:
            # вернуть всё найденное множество
            return found


    #
    def sort(self, sample: Dict) -> Dict:
        """
        Вернуть копию отсортированных данных.

        Сортировка производится в порядке: фамилия, имя, отчество, дата рождения.
        """

        return Dict((k, v) for k, v in sorted(sample.items(),
               key=lambda x: [x[1].last, x[1].first, x[1].middle, x[1].birth]))


    #
    @staticmethod
    def _rstr(s):
        """Преобразовать значение переменной к виду raw-string"""

        raw_map = {8:r'\b', 7:r'\a', 12:r'\f', 10:r'\n', 13:r'\r', 9:r'\t', 11:r'\v'}
        return r''.join(i if ord(i) > 32 else raw_map.get(ord(i), i) for i in s)
