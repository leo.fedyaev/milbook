# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import sys
import nestedtext as nt

from addict import Dict
from pathlib import Path
from rich import print as pprint

from Label import Label


#
class Config:
   """
   Класс для работы с конфигурационным файлом.

   Поля конфигурационного файла:
   - dirs: dict,директории проекта:
     - src: str, наименование директории, где располагаются карточки;
     - img: str, наименование директории, где располагаются фотографии к карточкам;
   """

   #
   @staticmethod
   def load(filename: str) -> Dict:
      """Загрузить данные из файла конфигурации"""

      if not Path(filename).exists():
         pprint(f'{Label.H.error} Файл конфигурации {filename} не найден;')
         sys.exit(-1)

      return Dict(nt.load(filename))
