# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

from addict import Dict
from functools import reduce
from rich import print as pprint

from Label import Label
from Card import Card
from Engine import Engine
from task.Task import Task


#
class InfoTask(Task):
    """Реализация задачи ~info~"""

    def run(self) -> None:
        super().run()

        def __reduce(cat: int) -> int:
            return reduce(lambda x, y: x + 1 if y.category == cat else 0,
                          self.data.values(), 0)

        pprint(Label.H.info, f'Распределение карточек по категориям:')

        print(f'\t- погибших на фронте:  {__reduce(0)};')
        print(f'\t- погибших от ранений: {__reduce(1)};')
        print(f'\t- без вести пропавших: {__reduce(2)};')
        print(f'\t- гражданских:         {__reduce(3)};')
