# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import sys
import nestedtext as nt

from addict import Dict
from pathlib import Path
from rich.progress import track
from rich import print as pprint
from pydantic import ValidationError

from Card import Card
from Label import Label
from Engine import Engine


#
class Task:
    """Базовый класс для выполняемой задачи"""

    def __init__(self, args: Dict, config: Dict):
        self.args = args
        self.config = config


    #
    def load(self) -> None:
        """Загрузить данные"""

        self.data = {}

        for p in track(Path(self.config.dirs.src).glob('*'),
                       description=self._track('Загрузка данных')):

            if not p.is_file():
                continue

            try:
                self.data[p.name] = Card(**nt.load(p))

            except ValidationError as e:
                pprint(f'{Label.H.error} Ошибка в файле', p.name)
                print(e)

                sys.exit(-1)

        if not len(self.data):
            pprint(Label.H.warn, 'Данные отсутствуют;')

        pprint(Label.H.done, f'Загружено {len(self.data)} карточек;')

        self.engine = Engine(self.data, self.args, self.config)


    #
    def foto(self, k: str) -> bool:
        """Существует ли фотография у данной карточки?"""

        return Path(f'{self.config.dirs.res}/{Path(k).stem}.png').exists()

    #
    def item(self, k: str, v: Card, verbose = False) -> str:
        """Полное текстовое представление одной карточки"""

        F = {True:  Label.I.exists, False: Label.I.skipped}

        res = f'{F[(self.foto(k))]} {k}:\n'

        if verbose:
            res += nt.dumps(v.model_dump())
        else:
            res += f'\t{v.text()};'

        return res


    #
    def run(self, load: bool = True) -> None:
        """Реализация логики задачи"""

        pprint(f'{Label.H.task} {self.args.task};')

        if load:
            self.load()


    #
    def _track(self, title: str) -> str:
        title += ':'

        return f'{Label.H.track} {title:<25}'
