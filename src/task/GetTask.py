# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import re

from addict import Dict
from rich.progress import track
from rich import print as pprint

from Card import Card
from Label import Label
from Engine import Engine
from task.Task import Task


#
class GetTask(Task):
    """Реализация задачи ~info~"""

    def run(self) -> None:
        super().run()

        pprint(f'{Label.H.info} Результаты:')

        found = self.engine.find()

        for k, v in track(found.items(),
                          description=self._track('Печать списка')):
            pprint(self.item(k, v, self.args.verbose))


        pprint(f'{Label.H.done} Найдено: {len(found)} из {len(self.data)} карточек;')
