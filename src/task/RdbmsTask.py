# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import psycopg2

from addict import Dict
from pathlib import Path
from rich import print as pprint
from base64 import b64decode
from jinja2 import Environment, FileSystemLoader

from Label import Label
from Card import Card
from Engine import Engine
from task.Task import Task


#
class RdbmsTask(Task):
    """Реализация задачи ~rdbms~"""

    _subtasks = ['info', 'init', 'update', 'delete']


    def run(self) -> None:
        super().run()

        subtasks = Dict(zip(RdbmsTask._subtasks,
                            [self._info,
                             self._init,
                             self._update,
                             self._delete,
                             ]))

        subtasks[self.args.subtask]()


    #
    def _info(self) -> None:
        """Проверка соединения и вывод основной статистической информации"""

        try:
            conn = psycopg2.connect(
                host=self.config.rdbms.connect.host,
                dbname=self.config.rdbms.connect.dbname,
                user=self.config.rdbms.connect.user,
                password=self.__passwd(self.config.rdbms.connect.password)
            )
        except Exception as e:
            pprint(Label.H.error, 'Ошибка подключения к базе данных:', e)
            return

        cursor = conn.cursor()
        cursor.execute(f'select * from {self.config.rdbms.table}')

        fetch = cursor.fetchall()

        pprint(Label.H.done, 'Соединение с базой данных установлено;')
        pprint(Label.H.info, f'Найдено {len(fetch)} записей в базе;')


    #
    def _init(self) -> None:
        """Созадть SQL-скрипт для создания базы данных и таблицы"""

        folder = f'{Path(__file__).parent.parent.parent}/res/sql/'
        env = Environment(loader=FileSystemLoader(folder))
        T = env.get_template('init.sql')

        filename = f'{self.config.dirs.build}/init.sql'

        if Path(filename).exists() and not self.args.force:
            pprint(Label.H.error, Label.F.exists(filename))
            return

        with open(filename, 'w') as f:
            f.write(T.render(dbname=self.config.rdbms.connect.dbname,
                             user=self.config.rdbms.connect.user,
                             table=self.config.rdbms.table))

        pprint(Label.H.done, f"Скрипт для создания базы данных '{filename}' создан;")


    #
    def _update(self) -> None:
        """Добавить/обновить записи в таблице соответствующие найденным карточкам"""

        sample = self.engine.find()


    #
    def _delete(self) -> None:
        pass


    #
    def __passwd(self, filename: str) -> str:
        """Считать пароль из файла и вернуть его в виде строки"""

        with open(filename.lstrip('<').rstrip('>'), 'r') as f:
            bline = f.readline().strip('\n')

        return b64decode(bline.encode('ascii')).decode('ascii')
