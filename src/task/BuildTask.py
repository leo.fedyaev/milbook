# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import shutil

from addict import Dict
from rich.progress import track
from rich import print as pprint
from pathlib import Path

from Label import Label
from Engine import Engine
from Format import LatexFormat
from task.Task import Task
from task.GetTask import GetTask


#
class BuildTask(GetTask):
    """Реализация задачи ~build~"""

    def run(self) -> None:
        Task.run(self)

        # удалить и потом создать директорию
        #shutil.rmtree(self.config.dirs.build)
        Path(self.config.dirs.build).mkdir(parents=True, exist_ok=True)

        # список доступных шаблонов
        res = f'{Path(__file__).parent.parent.parent}/res/latex'
        template = f'{res}/{self.args.template}.tex'

        # копировать шаблон
        shutil.copyfile(template, f'{self.config.dirs.build}/{self.args.target}.tex')

        # поиск
        found = self.engine.find()

        with open(f'{self.config.dirs.build}/build.inc', 'w') as f:
            for k, v in track(found.items(),
                              description=self._track('Форматирование карточек')):

                key = Path(k).stem \
                    if Path(f'{self.config.dirs.res}/{Path(k).stem}.png').exists() \
                       else ''

                text = LatexFormat(v).format(key)

                f.write(text)

                if self.args.verbose:
                    print(text, '\n')

        pprint(f'{Label.H.done} Записано: {len(found)} карточек;')
