# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import uuid
import nestedtext as nt
from rich import print as pprint

from Card import Card
from Label import Label
from task.Task import Task


#
class AddTask(Task):
    """Реализация задачи ~add~"""

    #
    def run(self) -> None:
        super().run(False)

        vals = ['', '', '']
        for i, v in enumerate(self.args.name):
            vals[i] = v

        fio = dict(zip(['last', 'first', 'middle'], vals))

        key = f'{uuid.uuid4()}.nt'
        card = Card(**fio)

        with open(f'{self.config.dirs.src}/{key}', 'w', encoding='utf-8') as f:
            nt.dump(card.model_dump(), f)

        pprint(f'{Label.H.done} карточка создана;')

        # загрузить данные
        self.load()

        # распечатать созданую карточку
        pprint(self.item(key, self.data[key], self.args.verbose))
