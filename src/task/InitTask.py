# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import re
import uuid
import nestedtext as nt

from addict import Dict
from pathlib import Path
from rich.progress import track
from rich import print as pprint

from Card import Card
from Label import Label
from task.Task import Task


#
class InitTask(Task):
    """Реализация задачи ~init~"""

    def run(self) -> None:
        super().run()

        names = []

        # наличие директории с карточками
        if Path(self.config.dirs.src).exists():
            pprint(f'{Label.H.error} Данные уже существуют!')
            return

        # список всех людей
        with open(self.args['list'], 'r') as f:
            while line := f.readline().strip('\n'):
                names.append(line)

        # создать директории
        Path(self.config.dirs.src).mkdir(parents=True, exist_ok=True)
        Path(self.config.dirs.res).mkdir(parents=True, exist_ok=True)

        #
        for name in track(sorted(names), 
                          description=self._track('Создание карточек')):
            parts = re.findall(r'([А-ЯЁа-яё-]+)+', name, re.U)
            card = Card(**{'last': parts[0].capitalize(),
                           'first': parts[1].capitalize(),
                           'middle': ' '.join(parts[2:])})

            with open(f'{self.config.dirs.src}/{uuid.uuid4()}.nt',
                      'w', encoding='utf-8') as f:
                nt.dump(card.model_dump(), f)

        pprint(f'{Label.H.done} Создано {len(names)} карточек;')
