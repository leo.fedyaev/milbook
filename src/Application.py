# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

import os
import sys
import argparse
import nestedtext as nt
from addict import Dict
from pathlib import Path
from rich import print as pprint

from Label import Label
from Config import Config
from task.InitTask import InitTask
from task.InfoTask import InfoTask
from task.GetTask import GetTask
from task.AddTask import AddTask
from task.BuildTask import BuildTask
from task.RdbmsTask import RdbmsTask


#
class Application:
    """
    Оператор персональных карточек для учёта и автоматической вёрстки
    """

    _tasks = ['init', 'info', 'get', 'build', 'add', 'rdbms',]
    _subtasks = RdbmsTask._subtasks + ['']

    def __init__(self, args: Dict):
        self.args = args
        self.config = Config.load(args.config)

        self.tasks = dict(zip(
            Application._tasks,
            [InitTask(self.args, self.config),
             InfoTask(self.args, self.config),
             GetTask(self.args, self.config),
             BuildTask(self.args, self.config),
             AddTask(self.args, self.config),
             RdbmsTask(self.args, self.config),
             ]))

    #
    def run(self, task: str) -> None:
        self.tasks[task].run()


#
#
#
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=Application.__doc__)

    # выполняемая задача
    parser.add_argument('task',
                        choices=Application._tasks,
                        default='info', nargs='?',
                        help='Задача, которая должна быть выполнена.')

    # подзадача (опционально), только для задачи rdbms
    parser.add_argument('subtask',
                        choices=Application._subtasks, nargs='?',
                        help='rdbms: Подзадача, которая должна быть выполнена.')

    # опции
    parser.add_argument('--config', type=str, default='project.conf',
                        help='целевой файл конфигурации')

    parser.add_argument('--list', default='', type=str, metavar='FILENAME',
                        help='init: файл со списком людей для создания карточек')

    parser.add_argument('--verbose', action='store_true',
                        help='get: выводить полную информацию из карточки')

    parser.add_argument('--name', nargs='+', default=[''], type=str, metavar='REGEXP',
                        help='add, get, build: регулярное выражения для поиска по фамилии, имени, отчеству')

    parser.add_argument('--gender', choices=['all', 'male', 'female'], default='all', nargs='?',
                        help='get, build: признак для поиска по половой принадлежности')

    parser.add_argument('--category', default=None, type=int,
                        help='get, build: признак для поиска по категории')

    parser.add_argument('--birth', default=None, type=str,
                        help='get, build: выражение для поиска по дате рождения')

    parser.add_argument('--death', default=None, type=str,
                        help='get, build: выражение для поиска по дате смерти')

    parser.add_argument('--age', default=None, type=str,
                        help='get, build: выражение для поиска по возрасту')

    parser.add_argument('--head', default=None, type=int,
                        help='get, build: первые несколько значений из списка')

    parser.add_argument('--tail', default=None, type=int,
                        help='get, build: последние несколько значений из списка')

    parser.add_argument('--not-empty', action='store_true',
                        help='get, build: включать в результаты поиска только заполненные карточки')

    parser.add_argument('--foto',
                        choices=['both', 'yes', 'no'], nargs='?', default='both',
                        help='get, build: включать в результаты поиска только заполненные карточки')

    parser.add_argument('--force', action='store_true',
                        help='rdbms: перезаписать существующий файл')


    # список доступных шаблонов
    res = f'{Path(__file__).parent.parent}/res/latex'
    templates = [p.stem for p in Path(res).glob('*.tex')]

    parser.add_argument('--template', choices=templates, default=templates[0],
                        help='build: наименование шаблона к использованию для вёрстки')

    # разбор опций и аргументов командной строки
    args = parser.parse_args()

    if args.subtask and args.task != 'rdbms':
        pprint(Label.H.warn, f"Подзадачи используются только для задачи 'rdbms', "
               f"подзадача '{args.subtask}' проигнорирована;")

    if args.task == 'rdbms' and not args.subtask:
        parser.print_usage()
        print(f"{Path(__file__).name}: error: argument subtask: invalid choice (choose from ",
              ', '. join(["'{}'".format(value) for value in RdbmsTask._subtasks]), "\b)")
        sys.exit(-1)

    Application(Dict(args.__dict__)).run(args.task)
