# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

from addict import Dict


#
class Label:
    """Набор стандартных записей для вывода информации"""

    H = Dict(
        info='[bold][INFO][/bold]',
        task='[bold][TASK][/bold]',
        error='[bold][red][STOP][/red][/bold]',
        warn='[bold][yellow][WARN][/yellow][/bold]',
        done='[bold][green][DONE][/green][/bold]',
        track='[bold][WAIT][/bold]'
    )

    R = Dict(
        done='[/green]успешно[/green]',
        skipped='[yellow]ранее обработан[/yellow]',
    )

    I = Dict(
        exists='[bold][green][+][/green][/bold]',
        skipped='[bold][red][-][/red][/bold]',
    )

    F = Dict(
        exists=lambda f: f"Файл '{f}' уже существует, для перезаписи используйте --force;"
    )
