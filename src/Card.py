# Автор: Леонид Федяев, (2022--2023) г.г.
# Лицензия: Apache-2.0 (см. LICENSE)

from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from pydantic import BaseModel, conint, field_serializer, validator


#
class Card(BaseModel):
    """Описание индивидуальной карточки"""

    last: str             # фамилия
    first: str            # имя
    middle: str           # отчество
    male: bool = True     # признак мужского пола (да, нет)
    birth: date = date(1900, 1, 1)
                          # дата рождения
    death: date = date(1910, 1, 1)
                          # дата смерти
    category: conint(ge=0, le=3) = 0
                          # категория {0: умерший на фронте, 1: умерший от ранений,
                          #            2: пропавший без вести, 3: гражданский}
    bio: str = ''         # краткая справка (биография)

    @field_serializer('birth', 'death')
    def dump_date(self, d: date, _info):
        return d.strftime('%d-%m-%Y')

    @validator('birth', 'death', pre=True)
    def load_date(cls, d):
        return datetime.strptime(d, '%d-%m-%Y').date()


    #
    def empty(self) -> bool:
        """Проверка на заполненность карточки"""

        return not (self.last and self.first and\
            (self.birth != date(1900, 1, 1)) and (self.death != date(1910, 1, 1)) and\
            (len(self.bio.strip()) > 0))


    #
    def text(self) -> str:
        """Упрощённое текстовое представление карточки"""

        def __date(d: date) -> str:
            return d.strftime('%d-%m-%Y')

        return f'({__date(self.birth)}, {__date(self.death)}) = {self.age()}: '\
            f'{self.last}, {self.first}, {self.middle}'


    #
    def age(self) -> int:
        """Количество полных лет (возраст)"""

        return relativedelta(self.death, self.birth).years
